package com.juanm.jms.listener;

import java.net.UnknownHostException;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;

import com.juanm.jms.adapter.ConsumerAdapter;

/**
 * Class represents and MDB to read from the consumer QUEUE
 * 
 * @author JuanMarcos
 */
@Component
public class ConsumerListener implements MessageListener {
	private static Logger logger = LogManager.getLogger(ConsumerListener.class);
	
	@Autowired
	JmsTemplate jmsTemplate; //This template is used to send the error message to the error queue
	
	@Autowired
	ConsumerAdapter consumerAdapter; //Adapter for the msg content to POJOs/Backend
	
	public void onMessage(Message message) {
		//At production we should set the log4j2.xml file logging level to WARN or ERROR to avoid crowding the server with files
		logger.info("In onMessage");
		
		//We are expecting to receive a string json format message
		String json = null;
		
		if(message instanceof TextMessage) {
			try {
				json = ((TextMessage) message).getText();
				logger.info("Sending JSON to DB: " + json);
				consumerAdapter.sendToMongoDB(json);
				
			} catch (JMSException e) {
				//If we have an exception with the message, put the message in the ERROR MSG QUEUE 
				logger.error("Failed Message " + json);
				jmsTemplate.convertAndSend(json);
			} catch (UnknownHostException e) {
				logger.error("Failed Persisting json Object to DB " + json);
				jmsTemplate.convertAndSend(json);
			} catch (Exception fe) {
				logger.error("Unexpected Exception encountered while working with object...");
				jmsTemplate.convertAndSend(json);
			}
		}
	}

}
