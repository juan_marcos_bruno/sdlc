package com.juanm.jms.adapter;

import java.net.UnknownHostException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;

import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.util.JSON;

/**
 * Class represents an adapter for the messages and the business/persistence
 * layers
 * 
 * @author JuanMarcos
 */
@Component
public class ConsumerAdapter {
	// Constant to refer to the DB name for the consumer
	private static final String VENDOR_DB = "Vendor";
	private static final String COLLECTION_NAME = "Contact";
	
	private static Logger logger = LogManager.getLogger(ConsumerAdapter.class);

	// Method ships the json to the datasource, in this specific case MongoDB
	public void sendToMongoDB(String json) throws UnknownHostException {
		logger.info("Sending to MongoDB...");
		// Instantiate a MongoDB client
		MongoClient mongoClient = new MongoClient();
		// Whatever name we put to fecht the DB, if it doesn't exist mongo will
		// create one, see Utest too
		DB db = mongoClient.getDB(VENDOR_DB);
		//Ge the Collection (table in SQL) object from the DB
		DBCollection collection = db.getCollection(COLLECTION_NAME);
		logger.info("Converting JSON string to DB object");
		
		//Parse with Mongo static object converter, and get the row (DBObject)
		DBObject object = (DBObject) JSON.parse(json);
		
		//Insert into mongoDB
		collection.insert(object);
		logger.info("Object Sent to MongoDB");
	}
}
