package test.com.juanm.jms.listener;

import static org.junit.Assert.*;
import static org.easymock.EasyMock.createMock;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.replay;
import static org.easymock.EasyMock.verify;

import javax.jms.JMSException;
import javax.jms.TextMessage;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.juanm.jms.listener.ConsumerListener;

public class ConsumerListenerTest {
	//Class variable to use in the tests, a simple txt message
	private TextMessage textMessage;
	//Application context that loads the spring config file for it with the beans
	private ApplicationContext testContext;
	//The consumer listener class
	private ConsumerListener consumerListener;
	//Json for the test
	private String json = "{vendorName:\"Microsofttest3\",firstName:\"BobTest3\",lastName:\"SmithTest3\",address:\"123 Main test3\",city:\"TulsaTest3\",state:\"OKTest3\",zip:\"71345Test3\",email:\"Bob@microsoft.test3\",phoneNumber:\"test-123-333\"}";
	
	@Before
	public void setUp() throws Exception {
		//Set the test context and inject the listener bean to this test property
		testContext = new ClassPathXmlApplicationContext("/spring/application-config.xml");
		//The MDB or listener for the test
		consumerListener = (ConsumerListener) testContext.getBean("consumerListener");
		//The message to be processed
		textMessage = createMock(TextMessage.class);
	}

	@After
	public void tearDown() throws Exception {
		//Close the test context, for this cast it before
		((ConfigurableApplicationContext)testContext).close();
	}

	//Method tests the onMessage on the MDB
	@Test
	public void testOnMessage() throws JMSException {
		//Expectations
		expect(textMessage.getText()).andReturn(json);
		replay(textMessage);
		consumerListener.onMessage(textMessage);
		//Verifications on the mocked object
		verify(textMessage);
	}

}
