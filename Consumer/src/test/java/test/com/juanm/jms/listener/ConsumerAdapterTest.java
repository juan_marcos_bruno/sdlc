package test.com.juanm.jms.listener;

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.juanm.jms.adapter.ConsumerAdapter;

/**
 * Class tests the ConsumerAdapter class of the backend. Assuming we will have a "SandBox" env for testing, this will be
 * and Integration Test functionally speaking
 * 
 * @author JuanMarcos
 */
public class ConsumerAdapterTest {
	//Json mock to be sent to DB
	private String json = "{vendorName:\"Microsofttest\",firstName:\"BobTest\",lastName:\"SmithTest\",address:\"123 Main test\",city:\"TulsaTest\",state:\"OKTest\",zip:\"71345Test\",email:\"Bob@microsoft.test\",phoneNumber:\"test-123-test\"}";
	
	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testSendToMongoDB() throws Exception {
		ConsumerAdapter consumerAdapter = new ConsumerAdapter();
		assertNotNull(json);
		consumerAdapter.sendToMongoDB(json);
	}

}
